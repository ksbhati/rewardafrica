//
//  CustomCellForTableTableViewCell.m
//  Africa
//
//  Created by Kunal Singh Bhati on 29/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "CustomCellForTableTableViewCell.h"

@implementation CustomCellForTableTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
