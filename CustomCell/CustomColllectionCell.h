//
//  CustomColllectionCell.h
//  Africa
//
//  Created by Kunal Singh Bhati on 14/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomColllectionCell : UICollectionViewCell 
@property (strong, nonatomic) IBOutlet UIImageView *lblImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitles;
@property (strong, nonatomic) IBOutlet UIImageView *imgDone;

@end
