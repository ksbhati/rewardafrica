//
//  callOutView.h
//  Africa
//
//  Created by Kunal Singh Bhati on 04/02/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface callOutView : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *viewCallOutShow;
@property (strong, nonatomic) IBOutlet UIImageView *imgOffer;
@property (strong, nonatomic) IBOutlet UILabel *titleOffer;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;


@end
