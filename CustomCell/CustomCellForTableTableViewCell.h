//
//  CustomCellForTableTableViewCell.h
//  Africa
//
//  Created by Kunal Singh Bhati on 29/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellForTableTableViewCell : UITableViewCell


// Home Screen
@property (strong, nonatomic) IBOutlet UIImageView *imageOffer;

@property (strong, nonatomic) IBOutlet UILabel *lblNewPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblOldPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblOfferValidTill;
@property (strong, nonatomic) IBOutlet UILabel *lblOfferDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblOffer;


// My Coupons

@property (strong, nonatomic) IBOutlet UIImageView *imageMyPointsVendor;
@property (strong, nonatomic) IBOutlet UILabel *lblMyPointsRedeem;
@property (strong, nonatomic) IBOutlet UILabel *lblMyPointsTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblMyPointsTotalPoints;

// MyPointsOffer // My Rewards

@property (strong, nonatomic) IBOutlet UILabel *lblMPOofferPoints;
@property (strong, nonatomic) IBOutlet UILabel *lblMPOofferTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblMPOofferDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblMPOofferValidTill;

// My Merchants

@property (strong, nonatomic) IBOutlet UILabel *lblMerchantNameList;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewMerchantList;
@property (strong, nonatomic) IBOutlet UIImageView *imageChecked;





@end
