//
//  Config.M
//  
//
//  Created by Oscar on 23/01/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.



#import "Config.h"  

@implementation Config

NSString *SiteAPIURL = @"http://codetesting.xyz/loyalty-africa/app/";

//NSString *SiteSecureKey = @"3k656k90asd3l489f024kj0d934j0dsfk3456h9d343l";

AppDelegate *app;
UIWindow *mainWindow;
UIStoryboard *mainStoryBoard;
UINavigationController *navController;

NSString *appDeviceToken = @"123";

BOOL isBackgroundMode = NO;

BOOL isDebuggerMode = NO;

BOOL isUserLogin = NO;
BOOL userLoginWithFB = NO;
NSString *userID = @"";
NSDictionary *userDict = nil;
NSString *alertMessage = @"";


// for GPS current location
float core_latitude = 0.0f, core_longitude = 0.0f;

BOOL isDeviceInBackgroundMode = NO;

// HUD
MBProgressHUD *HUD = nil;


void pt(NSString *format, ...) {
    if (isDebuggerMode) {
        va_list args;
        va_start(args, format);
        NSLogv(format, args);
        va_end(args);
    }
}

// custom alert

NSString *INAPPKey  = @"775bacc6d32b4b6ab81ae4e81286fd74";


NSString *catImageFolderPath = @"", *catImagePath = @"";
BOOL catfileExists = NO;

@end