//
//  Config.h
//  
//
//  Created by GDS on 23/01/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "GSFont.h"





#define DOCUMENT_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]


#define UIFontBellotaRegularSize(s) [UIFont fontWithName:@"HelveticaNeue" size:s]

#define UIFontBellotaBoldITSize(s) [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:s]

#define UIFontBellotaLightSize(s) [UIFont fontWithName:@"HelveticaNeue-Light" size:s]

#define UIFontBellotaBoldSize(s) [UIFont fontWithName:@"HelveticaNeue-Bold" size:s]

#define UIFontBellotaITSize(s) [UIFont fontWithName:@"HelveticaNeue-Italic" size:s]


#define IS_iPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define IS_iPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define DELEGATE ((AppDelegate*)[[UIApplication sharedApplication]delegate])



#ifdef __IPHONE_7_0
# define STATUS_STYLE UIStatusBarStyleLightContent
#else
# define STATUS_STYLE UIStatusBarStyleBlackTranslucent
#endif


#define pr(str,args...) NSLog(str,args);
#define pro(str)        NSLog(@"%@",str);
#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

#define APP_REVIEW_NEVER_ASK -1
#define APP_REVIEW_ASK_AFTER 5

#define CORNER_RADIUS  19.5f

//#define INAPPKey

@interface Config : NSObject {

    
}


//configuration section...
extern  NSString            *SiteAPIURL;
//extern  NSString            *SiteSecureKey;

extern AppDelegate *app;
extern UIWindow *mainWindow;
extern UIStoryboard *mainStoryBoard;
extern UINavigationController *navController;

extern NSDictionary *userDict;

extern  NSString *appDeviceToken;
extern  NSString *alertMessage;
extern  NSString *userID;
extern  NSString *INAPPKey;
extern  NSString *catImageFolderPath, *catImagePath;

extern BOOL isBackgroundMode;
extern BOOL isDebuggerMode;
extern BOOL isUserLogin;
extern BOOL userLoginWithFB;
extern BOOL isDeviceInBackgroundMode;


//for GPS current location
extern float core_latitude, core_longitude;

// hud
extern MBProgressHUD *HUD;

void pt(NSString *format, ...);


extern BOOL catfileExists;

@end
