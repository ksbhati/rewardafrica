//
//  MenuViewController.h
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface LeftMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    
    IBOutlet UILabel *lblUserProfileName;
    IBOutlet UIButton *btnProfile;
    IBOutlet UIImageView *imageViewProfile;
}
- (IBAction)btnProfile:(id)sender;
- (IBAction)btnLogout:(id)sender;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@end
