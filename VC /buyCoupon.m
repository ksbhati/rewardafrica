//
//  buyCoupon.m
//  Africa
//
//  Created by Kunal Singh Bhati on 28/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import "buyCoupon.h"


@interface buyCoupon ()

@end

@implementation buyCoupon

@synthesize couponID,buyAmount;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    txtTotalAmount.text = [NSString stringWithFormat:@"Total billing amount :$%@",buyAmount];
    
    [self initPaypal];
}

#pragma mark - textField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}

// Web service for not buying coupons
- (void)sendPaymentInfoToServer {
    
    //http://codetesting.xyz/loyalty-africa/app/coupon.php?action=use&coupon_id=3&u_id=1&coupon_redeem_type=buy_now
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"coupon.php?action=use&coupon_id=%@&u_id=%@&coupon_redeem_type=buy_now",couponID,userID];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        [CommonFunctions AlertWithMsg:@"Purchase Successfully.You can redeem this coupon from My Coupon."];
                           [self dismissViewControllerAnimated:NO completion:nil];
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

- (BOOL)validateForm {
    
    if ([Validate isNull:txtNameOnCard.text]) {
        [CommonFunctions AlertWithMsg:@"Please enter valid Name"];
    } else if (![Validate isValidCreditCardNumber:txtCardNumber.text]) {
        [CommonFunctions AlertWithMsg:@"Please enter valid Credit Card number"];
    }  else if ([Validate isNull:txtExpiryDate.text]) {
        [CommonFunctions AlertWithMsg:@"Please select card expiry date"];
    } else if (![Validate isValidCCV:txtCVV.text]) {
        [CommonFunctions AlertWithMsg:@"Please enter valid CCV number"];
    } else {
        return YES;
    }
    return NO;
}


- (IBAction)btnPay:(id)sender {
    
//    NSString *totalAmount = [NSString stringWithFormat:@"%.2lf", actualBillAmount];
    NSString *itemDetail = [NSString stringWithFormat:@"PikaDily Coupon Payment:"];
    PayPalItem *item1 = [PayPalItem itemWithName:itemDetail
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:buyAmount]
                                    withCurrency:@"USD"
                                         withSku:couponID];
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = @"USD";
    payment.shortDescription = itemDetail;
    payment.items = items;
    payment.paymentDetails = paymentDetails;
    
    self.payPalConfig.acceptCreditCards = YES;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

- (IBAction)btnClose:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
// Pay Pal
#pragma mark - Paypal Code here

- (void)initPaypal {
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"ASpcKnRs1ZVS5oVi9mHgtC4N-ZNSrDXz2WpprQS-ZH5FCtoo7fy2YWO9KqiCc-FxrL5tx0Mo5g2Tyt8E",
                                                           PayPalEnvironmentSandbox : @"AUfggXaYAmA7MQXIZVvu5jyU6xxX1LFV4VkXov15lkqkUUIBDz_NUs6y-Um1DSrHok3CkEVMvJLboEyK"}];
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
    
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = YES;
    _payPalConfig.merchantName = @"PikaDily";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
}


#pragma mark - PayPalPaymentDelegate methods
- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!%@", [completedPayment description]);
    [self sendCompletedPaymentToServer:completedPayment];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark send payment to server
- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    
    if ([[completedPayment.confirmation[@"response"] objectForKey:@"state"] isEqualToString:@"approved"]){
        
    }
}


@end
