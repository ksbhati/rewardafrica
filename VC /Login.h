//
//  Login.h
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface Login : UIViewController{
    
    IBOutlet UIImageView *imgeViewTopPage;
    IBOutlet UIView *viewSlider;
    
    IBOutlet UIView *viewLoginSlider;
    IBOutlet UIView *viewSignup;
    
    
    IBOutlet UIButton *btnLoginScreen;
    IBOutlet UIButton *btnSignUpScreen;
    
    
// Login
    
    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtPassword;
    
    
//Registeration
    
    IBOutlet UITextField *txtRegUsername;
    IBOutlet UITextField *txtRegPassword;
    IBOutlet UITextField *txtRegRePassword;
    IBOutlet UITextField *txtRegEmailAddress;
    IBOutlet UITextField *txtRegAge;
    IBOutlet UITextField *txtRegPostalCode;
    
    
}

- (IBAction)btnLoginPage:(id)sender;
- (IBAction)btnSignUpPage:(id)sender;

// Login
@property (strong, nonatomic) IBOutlet FBSDKLoginButton *loginfb;
- (IBAction)btnConnectFacebook:(id)sender;
- (IBAction)btnForgotPassword:(id)sender;
- (IBAction)btnLogin:(id)sender;

// Registeration

- (IBAction)btnRegisteration:(id)sender;
- (IBAction)addMerchants:(id)sender;







@end
