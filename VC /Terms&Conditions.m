//
//  Terms&Conditions.m
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "Terms&Conditions.h"

@interface Terms_Conditions ()

@end

@implementation Terms_Conditions

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnMenu:(id)sender {
    
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
    
}

@end
