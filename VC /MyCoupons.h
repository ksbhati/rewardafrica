//
//  MyCoupons.h
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCoupons : UIViewController{
    
    IBOutlet UITableView *tblView;
    IBOutlet UITextField *txtSearch;
    
}

- (IBAction)btnMenu:(id)sender;
- (IBAction)btnMapView:(id)sender;

@end
