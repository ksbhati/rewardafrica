//
//  ForgotPassword.h
//  Africa
//
//  Created by Kunal Singh Bhati on 04/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassword : UIViewController{
    
    IBOutlet UITextField *txtForgotPAssword;
    
}
- (IBAction)btnGetWebService:(id)sender;
- (IBAction)btnBack:(id)sender;

@end
