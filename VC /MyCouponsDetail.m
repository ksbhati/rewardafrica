//
//  MyCouponsDetail.m
//  Africa
//
//  Created by Kunal Singh Bhati on 30/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "MyCouponsDetail.h"

@interface MyCouponsDetail (){
    
    NSArray *arrTransfer;
}

@end

@implementation MyCouponsDetail
@synthesize couponIDForMyCouponDetail;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(homeDetail)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    
}


#pragma mark - Webservice
- (void)homeDetail {
    
    //    http://codetesting.xyz/loyalty-africa/app/get.php?action=coupon-detail&coupon_id=2&u_id=15
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=coupon-detail&coupon_id=%@&u_id=%@",couponIDForMyCouponDetail,userID];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            arrTransfer = dic[@"Data"];
                            
                            lblTitleForDetail.text = [[dic[@"Data"]valueForKey:@"title"]objectAtIndex:0];
                            
                            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageOffer];
                            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageViewBarCode];
                            
                            NSURL *imgURL = [NSURL URLWithString:[[dic[@"Data"]valueForKey:@"coupon_image"] objectAtIndex:0]];
                            imageOffer.imageURL = imgURL;
                            
                            NSURL *imgURL2 = [NSURL URLWithString:[[dic[@"Data"]valueForKey:@"qr_code_image"] objectAtIndex:0]];
                            imageViewBarCode.imageURL = imgURL2;
                            
                            lblNewPrice.text=[NSString stringWithFormat:@"$%@",[[dic[@"Data"]valueForKey:@"new_price"]objectAtIndex:0]];
                            lblOldPrice.text=[NSString stringWithFormat:@"$%@",[[dic[@"Data"]valueForKey:@"old_price"]objectAtIndex:0]];
                            
                            lblPoints.text  = [[dic[@"Data"]valueForKey:@"bonus_point"]objectAtIndex:0];
                            
                            lblOfferTitle.text = [[dic[@"Data"]valueForKey:@"title"]objectAtIndex:0];
                            lblDescription.text= [[dic[@"Data"]valueForKey:@"description"]objectAtIndex:0];
                            lblExpDate.text    = [NSString stringWithFormat:@"Valid Till : %@",[[dic[@"Data"]valueForKey:@"valid_till"]objectAtIndex:0]];
                            
                            lblRestName.text   = [[dic[@"Data"]valueForKey:@"merchant_name"]objectAtIndex:0];
                            lblAddLine1.text   = [[dic[@"Data"]valueForKey:@"merchant_address"]objectAtIndex:0];
                            
                            
                            lblPrice.text      = [NSString stringWithFormat:@"Price :$%@",[[dic[@"Data"]valueForKey:@"new_price"]objectAtIndex:0]];
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}


- (IBAction)btnMapView:(id)sender {
    
    if(app.flagForFront){
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        
        MapLocations *MapLocations = [self.storyboard instantiateViewControllerWithIdentifier:@"MapLocations"];
        
        MapLocations.arrVal = [arrTransfer mutableCopy];
        MapLocations.fromDetailPage = YES;
        
        [self.navigationController pushViewController:MapLocations animated:YES];
    }

}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
