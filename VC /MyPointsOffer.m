//
//  MyPointsOffer.m
//  Africa
//
//  Created by Kunal Singh Bhati on 31/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "MyPointsOffer.h"
#import "MyPointsOfferDetail.h"

@interface MyPointsOffer (){
    
    NSArray *arrTableData ,*filteredArray,*totalArray;
    BOOL isSearching;
    
}

@end

@implementation MyPointsOffer
@synthesize merchantID,merchantPoints;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(myPointsOffer)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)aTextField
{
    [aTextField resignFirstResponder];
    return YES;
    
}


-(void)textFieldDidChange:(UITextField*)textField
{
    NSString *substring = [NSString stringWithString:textField.text];
    
    [self searchAutocompleteEntriesWithSubstring:substring];
    
}


- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    if (txtSearch.text.length > 0) {
        isSearching = YES;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"reward_title contains[cd] %@",
                                        substring];
        
        filteredArray = [totalArray filteredArrayUsingPredicate:resultPredicate];
        arrTableData = [filteredArray mutableCopy];
        [tblView reloadData];
    }else{
        isSearching = NO;
        arrTableData = [totalArray mutableCopy];
        [tblView reloadData];
    }
    
}

#pragma mark - Webservice

- (void)myPointsOffer {
    
//    http://codetesting.xyz/loyalty-africa/app/get.php?action=reward-list&merchant_id=2&u_id=1
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=reward-list&merchant_id=%@&u_id=%@",merchantID,userID];
    
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            
                            totalArray = dic[@"Data"];
                            arrTableData = [totalArray mutableCopy];
                            filteredArray = [NSMutableArray arrayWithCapacity:[dic[@"Data"] count]];
                            [tblView reloadData];
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                       [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

#pragma mark - UI Table view Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrTableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"MyPointsOffer";
    
    CustomCellForTableTableViewCell *cell = [tblView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[CustomCellForTableTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
       
    }
    
    cell.lblMPOofferPoints.text = [[arrTableData valueForKey:@"reward_point"] objectAtIndex:indexPath.row];
    cell.lblMPOofferTitle.text = [[arrTableData valueForKey:@"reward_title"] objectAtIndex:indexPath.row];
    cell.lblMPOofferDescription.text = [[arrTableData valueForKey:@"reward_description"] objectAtIndex:indexPath.row];
    cell.lblMPOofferValidTill.text = [NSString stringWithFormat:@"Valid Till : %@",[[arrTableData valueForKey:@"reward_valid_till"] objectAtIndex:indexPath.row]];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    MyPointsOfferDetail *MyPointsOfferDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPointsOfferDetail"];
    
    MyPointsOfferDetail.rewardID = [[arrTableData valueForKey:@"reward_id"] objectAtIndex:indexPath.row];
    MyPointsOfferDetail.merchantPoints = merchantPoints;
    [self.navigationController pushViewController:MyPointsOfferDetail animated:YES];
    
    
}

#pragma mark - IBAction

- (IBAction)btnBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
