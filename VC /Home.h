//
//  Home.h
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Home : UIViewController{
    
    IBOutlet UITableView *tblView;
    IBOutlet UIButton *btnMapView;
    IBOutlet UIButton *btnLocation;
    IBOutlet UITextField *txtSearch;
       
}

- (IBAction)btnMenu:(id)sender;
- (IBAction)btnMapView:(id)sender;
- (IBAction)btnCurrentLocation:(id)sender;
- (IBAction)btnGo:(id)sender;



@end
