//
//  MyCouponsDetail.h
//  Africa
//
//  Created by Kunal Singh Bhati on 30/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCouponsDetail : UIViewController{
    
    IBOutlet UILabel *lblTitleForDetail;
    
    IBOutlet UIImageView *imageOffer;
    IBOutlet UILabel *lblNewPrice;
    IBOutlet UILabel *lblOldPrice;
    IBOutlet UILabel *lblPoints;
    
    IBOutlet UILabel *lblOfferTitle;
    IBOutlet UITextView *lblDescription;
    IBOutlet UILabel *lblExpDate;
    
    IBOutlet UIImageView *imageViewBarCode;
    
    IBOutlet UILabel *lblRestName;
    IBOutlet UILabel *lblAddLine1;
    IBOutlet UILabel *lblAddLine2;
    
    IBOutlet UILabel *lblPrice;
    
}

@property NSString *couponIDForMyCouponDetail;

- (IBAction)btnMapView:(id)sender;
- (IBAction)btnBack:(id)sender;


@end
