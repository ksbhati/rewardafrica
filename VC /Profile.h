//
//  Profile.h
//  Africa
//
//  Created by Kunal Singh Bhati on 31/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftMenuViewController.h"
#import "SlideNavigationController.h"


@interface Profile : UIViewController{
    
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblUserName;
    IBOutlet UIImageView *imgView;
    
    IBOutlet UITextField *lblNewPassword;
    IBOutlet UITextField *lblRePassword;
    IBOutlet UITextField *lblOldPassword;
    
    IBOutlet UITextField *txtEditName;
    
    IBOutlet UIView *prefV;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *viewEnterName;
}

- (IBAction)btnGotoPreferences:(id)sender;

- (IBAction)btnMenu:(id)sender;
- (IBAction)btnEditName:(id)sender;
- (IBAction)btnUploadImage:(id)sender;
- (IBAction)btnUpdateProfile:(id)sender;
- (IBAction)btnOK:(id)sender;
- (IBAction)btnCancel:(id)sender;
- (IBAction)btnSetCircleOfFriends:(id)sender;


@end
