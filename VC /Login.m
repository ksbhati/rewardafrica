//
//  Login.m
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "Login.h"
#import "ForgotPassword.h"
#import "Preferences.h"
#import "ListMerchants.h"

@interface Login (){
    NSString *appLogin;
    NSString *fbFname,*fbLname,*fbEmail,*fbImageUrl,*loginType;
}

@end

@implementation Login

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Login
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    appLogin = [userDefaults objectForKey:@"isLogin"];
    
    if (![appLogin isEqualToString:@"NO"]) {
        isUserLogin = [CommonFunctions isUserLogin];
        
        if (isUserLogin) {
            
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Home"] animated:NO];
            return;
            
            // Registeration
        }
    }
}
-(void)viewWillAppear:(BOOL)animated{
    if (app.isRegistered) {
        
        txtRegUsername.text = @"";
        txtRegRePassword.text = @"";
        txtRegPassword.text = @"";
        txtRegAge.text = @"";
        txtRegPostalCode.text = @"";
        txtRegEmailAddress.text=@"";
        
        [btnLoginScreen sendActionsForControlEvents:UIControlEventTouchUpInside];
        
    }
    
}

- (void)getLoginDataFromServer{
    
    //    http://codetesting.xyz/loyalty-africa/app/app_login.php?username=loyaltydemo&password=admin
    
    NSString *strUrl;
    if ([app.loginType isEqualToString:@"0"]) {
        
        strUrl=[SiteAPIURL stringByAppendingFormat:@"app_login.php?logintyp=0&username=%@",fbEmail];
        
    }else{
        
        strUrl=[SiteAPIURL stringByAppendingFormat:@"app_login.php?logintyp=1&username=%@&password=%@",txtUserName.text,txtPassword.text];
    }
    
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            
                            //// login Successfull ////
                            isUserLogin = YES;
                            
                            userDict = dic[@"Data"];
                            userID   = [dic[@"Data"]valueForKey:@"id"];
                            
                            [CommonFunctions userLoginDetail:dic[@"Data"]];
                            
                            NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
                            [defs setObject:@"YES" forKey:@"isLogin"];
                            [defs synchronize];
                            
                            Home *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
                            [self.navigationController pushViewController:homeVC animated:YES];
                            
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"faliure"])
                    {
                        isUserLogin = NO;
                        
                        if ([dic[@"Message"]isEqualToString:@"User is not Registered."]) {
                            if ([app.loginType isEqualToString:@"0"]) {
                                
                                [self registerFacebook];
                                //                                [mainWindow addSubview:HUD];
                                //                                [HUD showWhileExecuting:@selector(registerFacebook)
                                //                                               onTarget:self
                                //                                             withObject:nil
                                //                                               animated:YES];
                            }
                            
                        }
                        else{
                            [CommonFunctions AlertWithMsg:dic[@"Message"]];
                        }
                        
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

-(void)registerFacebook {
    
    //http://codetesting.xyz/loyalty-africa/app/app_register.php?logintyp=0&username=yellowpeak@gmail.com&email=yellowpeak@gm
    
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"app_register.php?logintyp=0&username=%@&email=%@",fbEmail,fbEmail];
    
    pr(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:[strUrl stringByURLEncode]]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    
    if (!error && jsonStr) {
        NSError *error;
        NSDictionary *dic= [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]
                                                           options:NSJSONReadingMutableContainers
                                                             error:&error];
        pr(@"%@, error %@",dic,error);
        if (dic && !error)
        {
            // play with dic : server response
            if ([dic[@"Response"] isEqualToString:@"true"]) {
                
                [CommonFunctions AlertWithMsg:@"Registered Successfully!!!"];
                
                //// login Successfull ////
                isUserLogin = YES;
                userDict = dic[@"Data"];
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setObject:@"YES" forKey:@"isLogin"];
                
                [CommonFunctions userLoginDetail:dic[@"Data"]];
                
                Home *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
                [self.navigationController pushViewController:homeVC animated:YES];
                
            } else if ([dic[@"Response"] isEqualToString:@"failure"]) {
                
                [CommonFunctions AlertWithMsg:dic[@"Message"]];
                
            } else {
                
                [CommonFunctions serverInternalError];
                
            }
        } else {
            
            [CommonFunctions showServerNotFoundError];
        }
    }
    else{
        
        [CommonFunctions showServerNotFoundError];
    }
    
}


-(void)registerUser{
    
    //  http://codetesting.xyz/loyalty-africa/app/app_register.php?username=abit10004&email=abit10004@gmail.com&category=1,2&password=1234&age=18&postal_code=302019
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"app_register.php?logintyp=1&username=%@&email=%@&category=&password=%@&age=%@&postal_code=%@",txtRegUsername.text,txtRegEmailAddress.text,txtRegPassword.text,txtRegAge.text,txtRegPostalCode.text];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"true"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            [CommonFunctions AlertWithMsg:@"Registeration done successfully!!! Please check your email to Activate you account"];
                            [CommonFunctions AlertWithMsg:dic[@"Message"]];
                            txtRegUsername.text = @"";
                            txtRegRePassword.text = @"";
                            txtRegPassword.text = @"";
                            txtRegAge.text = @"";
                            txtRegPostalCode.text = @"";
                            
                            [btnLoginScreen sendActionsForControlEvents:UIControlEventTouchUpInside];
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"false"])
                    {
                        [CommonFunctions AlertTitle:@"Offers" withMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertTitle:@"Offers" withMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                //                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

#pragma mark - Validation for User

- (BOOL)validateFormUser {
    
    if ([Validate isNull:[txtRegUsername.text trim]]) {
        [CommonFunctions AlertWithMsg:@"Please enter User Name."];
    }  else if (![Validate isValidEmailId:[txtRegEmailAddress.text trim]]) {
        [CommonFunctions AlertWithMsg:@"Please enter valid email address"];
    } else if ([Validate isNull:[txtRegAge.text trim]]) {
        [CommonFunctions AlertWithMsg:@"Please enter Age."];
    } else if ([Validate isNull:[txtRegPostalCode.text trim]]) {
        [CommonFunctions AlertWithMsg:@"Please enter Zipcode"];
    }
    else {
        return YES;
    }
    return NO;
}


#pragma mark - Textfield Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -textField.frame.origin.y; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark - IBAction

- (IBAction)btnLoginPage:(id)sender {
    imgeViewTopPage.image = [UIImage imageNamed:@"login_icon"];
    
    //    Bottom Slider
    
    UIView* view = [self.view viewWithTag:200];
    [UIView animateWithDuration:0.6
                          delay:0.1
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = view.frame;
         frame.origin.y = 160;
         frame.origin.x = 0;
         view.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
    
    //    Pink Slider
    
    UIView* view1 = [self.view viewWithTag:100];
    [UIView animateWithDuration:0.6
                          delay:0.1
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = view1.frame;
         frame.origin.y = 157;
         frame.origin.x = 0;
         view1.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         // Orange Color
         [btnLoginScreen setTitleColor:[UIColor colorWithRed:230.0/256.0 green:124.0/256.0 blue:95.0/256.0 alpha:1.0] forState:UIControlStateNormal];
         // Grey Color
         [btnSignUpScreen setTitleColor:[UIColor colorWithRed:171.0/256.0 green:167.0/256.0 blue:166.0/256.0 alpha:1.0] forState:UIControlStateNormal];
         
         
     }];
    
}


- (IBAction)btnSignUpPage:(id)sender {
    
    imgeViewTopPage.image = [UIImage imageNamed:@"signup_icon"];
    
    // Bottom Slider
    UIView* view = [self.view viewWithTag:200];
    [UIView animateWithDuration:0.6
                          delay:0.1
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = view.frame;
         frame.origin.y = 160;
         frame.origin.x = -320;
         view.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
         
     }];
    
    // Pink Slider
    
    UIView* view1 = [self.view viewWithTag:100];
    [UIView animateWithDuration:0.6
                          delay:0.1
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = view1.frame;
         frame.origin.y = 157;
         frame.origin.x = 160;
         view1.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         
         // Orange Color
         [btnSignUpScreen setTitleColor:[UIColor colorWithRed:230.0/256.0 green:124.0/256.0 blue:95.0/256.0 alpha:1.0] forState:UIControlStateNormal];
         
         // Grey Color
         [btnLoginScreen setTitleColor:[UIColor colorWithRed:171.0/256.0 green:167.0/256.0 blue:166.0/256.0 alpha:1.0] forState:UIControlStateNormal];
         
     }];
    
    
}

// Login
- (IBAction)btnLogin:(id)sender {
    
    app.loginType = @"1";
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(getLoginDataFromServer)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    
    
}

- (IBAction)btnForgotPassword:(id)sender {
    
    ForgotPassword *ForgotPassword = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPassword"];
    [self.navigationController pushViewController:ForgotPassword animated:YES];
    
}

- (IBAction)btnConnectFacebook:(id)sender {
    app.loginType = @"0";
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorBrowser;
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if (error)
         {
             // Process error
         }
         else if (result.isCancelled)
         {
             // Handle cancellations
         }
         else
         {
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 NSLog(@"result is:%@",result);
                 [self fetchUserInfo];
                 [login logOut]; // Only If you don't want to save the session for current app
             }
         }
     }];
    
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 
                 //                 NSLog(@"result is:%@",result);
                 fbEmail = [result valueForKey:@"email"];
                 fbFname = [result valueForKey:@"first_name"];
                 fbLname = [result valueForKey:@"last_name"];
                 fbImageUrl = [[[result valueForKey:@"picture"]valueForKey:@"data"]valueForKey:@"url"];
                 [mainWindow addSubview:HUD];
                 [HUD showWhileExecuting:@selector(getLoginDataFromServer)
                                onTarget:self
                              withObject:nil
                                animated:YES];
                 
             }
             else
             {
                 NSLog(@"Error %@",error);
             }
         }];
    }
}



// Registeration

- (IBAction)btnRegisteration:(id)sender {
    
    [self.view endEditing:YES];
    if ([self validateFormUser]) {
        if ([txtRegPassword.text isEqualToString:txtRegRePassword.text]) {
            
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle:nil];
            ListMerchants *ListMerchants =
            [storyboard instantiateViewControllerWithIdentifier:@"ListMerchants"];
            
            
            ListMerchants.userName = txtRegUsername.text ;
            ListMerchants.password = txtRegPassword.text ;
            ListMerchants.emailAddress = txtRegEmailAddress.text;
            ListMerchants.postalCode = txtRegPostalCode.text;
            ListMerchants.age = txtRegAge.text;
            
            
            [self presentViewController:ListMerchants
                               animated:YES
                             completion:nil];
            
            //            [mainWindow addSubview:HUD];
            //            [HUD showWhileExecuting:@selector(registerUser)
            //                           onTarget:self
            //                         withObject:nil
            //                           animated:YES];
            
        }else{
            
            [CommonFunctions AlertWithMsg:@"Password doesn't match"];
            
        }
    }
    
    
}

- (IBAction)addMerchants:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    ListMerchants *ListMerchants =
    [storyboard instantiateViewControllerWithIdentifier:@"ListMerchants"];
    
    [self presentViewController:ListMerchants
                       animated:YES
                     completion:nil];
    
}
@end
