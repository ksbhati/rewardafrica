//
//  MyRewards.m
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "MyRewards.h"
#import "MyPointsOfferDetail.h"
#import "MyRewardDetailViewController.h"

@interface MyRewards (){
    
    NSArray *arrTableData ,*filteredArray,*totalArray;
    BOOL isSearching;

}

@end

@implementation MyRewards

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(myrewards)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    
}
#pragma mark - textField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)aTextField
{
    [aTextField resignFirstResponder];
    return YES;
    
}

-(void)textFieldDidChange:(UITextField*)textField
{
    NSString *substring = [NSString stringWithString:textField.text];
    
    [self searchAutocompleteEntriesWithSubstring:substring];
    
}


- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    if (txtSearch.text.length > 0) {
        isSearching = YES;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"merchant_name contains[cd] %@",
                                        substring];
        
        filteredArray = [totalArray filteredArrayUsingPredicate:resultPredicate];
        arrTableData = [filteredArray mutableCopy];
        [tblView reloadData];
        
    }else{
        
        isSearching = NO;
        arrTableData = [totalArray mutableCopy];
        [tblView reloadData];
        
    }
    
}



#pragma mark - Webservice

- (void)myrewards {
    
    //  http://codetesting.xyz/loyalty-africa/app/get.php?action=my-rewards&u_id=4
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=my-rewards&u_id=%@",userID];
    
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            
                            totalArray = dic[@"Data"];
                            arrTableData = [totalArray mutableCopy];
                            filteredArray = [NSMutableArray arrayWithCapacity:[dic[@"Data"] count]];
                            [tblView reloadData];
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                            [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}


#pragma mark - UI Table view Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrTableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"MyRewards";
    
    CustomCellForTableTableViewCell *cell = [tblView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[CustomCellForTableTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imageOffer];
    }
    
    cell.lblMPOofferPoints.text = [[arrTableData valueForKey:@"reward_point"] objectAtIndex:indexPath.row];
    cell.lblMPOofferTitle.text = [[arrTableData valueForKey:@"reward_title"] objectAtIndex:indexPath.row];
    cell.lblMPOofferDescription.text = [[arrTableData valueForKey:@"reward_description"] objectAtIndex:indexPath.row];
    cell.lblMPOofferValidTill.text = [NSString stringWithFormat:@"Valid Till : %@",[[arrTableData valueForKey:@"reward_valid_till"] objectAtIndex:indexPath.row]];
    
   
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyRewardDetailViewController *MyRewardDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyRewardDetailViewController"];
    
    MyRewardDetailViewController.rewardID = [[arrTableData valueForKey:@"reward_id"] objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:MyRewardDetailViewController animated:YES];
    
}

#pragma mark - IBAction

- (IBAction)btnMenu:(id)sender {
    
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
    
}

@end
