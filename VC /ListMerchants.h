//
//  ListMerchants.h
//  Africa
//
//  Created by Kunal Singh Bhati on 22/02/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListMerchants : UIViewController{
    
    IBOutlet UITableView *tblView;
    IBOutlet UITextField *txtSearchList;
    
}

- (IBAction)btnClose:(id)sender;
- (IBAction)btnOk:(id)sender;

@property NSString *userName,*password,*name,*emailAddress,*age,*postalCode;


@end
