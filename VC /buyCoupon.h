//
//  buyCoupon.h
//  Africa
//
//  Created by Kunal Singh Bhati on 28/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"

@interface buyCoupon : UIViewController<PayPalPaymentDelegate>{
    
    
    IBOutlet UITextField *txtCardNumber;
    IBOutlet UITextField *txtNameOnCard;
    IBOutlet UITextField *txtExpiryDate;
    IBOutlet UITextField *txtCVV;
    
    IBOutlet UILabel *txtTotalAmount;
}

@property (strong,nonatomic) NSString *couponID;
@property (strong,nonatomic) NSString *buyAmount;
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;


- (IBAction)btnPay:(id)sender;
- (IBAction)btnClose:(id)sender;

@end
