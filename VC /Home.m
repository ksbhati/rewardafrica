//
//  Home.m
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "Home.h"
#import "HomeDetailView.h"
#import "MapLocations.h"
#import <CoreLocation/CoreLocation.h>
#import "Preferences.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface Home ()<CLLocationManagerDelegate> {
    
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSString *searchLocation,*Lat,*Long;
    NSMutableArray *arrTableData;
    BOOL usingCurrentLocation;
}

@end

@implementation Home

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    Lat = @"";
    Long= @"";
    
    usingCurrentLocation = NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"bindMenuTitle" object:nil];

    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [btnLocation setBackgroundImage:[UIImage imageNamed:@"location_icon"] forState:UIControlStateNormal];
    [btnMapView setBackgroundImage:[UIImage imageNamed:@"map_view_icon"] forState:UIControlStateNormal];

    if (![userDict[@"user_preferences"]isKindOfClass:[NSArray class]]) {
        
        app.fromLogin = @"TRUE";
        
        Home *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Preferences"];
        [self.navigationController pushViewController:homeVC animated:YES];
        
    }else{
        
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(HomeOffers)
                       onTarget:self
                     withObject:nil
                       animated:YES];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - textField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}


#pragma mark - Webservice

- (void)searchOffers {
    
    // http://codetesting.xyz/loyalty-africa/app/get.php?action=coupon-list&search=Lorem
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=coupon-list&search=%@",txtSearch.text];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            arrTableData = [NSMutableArray new];
                            arrTableData = dic[@"Data"];
                            [tblView reloadData];
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}


- (void)HomeOffers {
    
    // http://codetesting.xyz/loyalty-africa/app/get.php?action=coupon-list&dist=0&lat=&long=
    
    NSString *strUrl;
    
    // Without Location
    if(usingCurrentLocation){
    strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=coupon-list&dist=1&lat=%@&long=%@",Lat,Long];
    }else{
    strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=coupon-list&dist=0&lat=&long="];
    }
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            
                            arrTableData = [NSMutableArray new];
                            arrTableData = dic[@"Data"];

//                            arrMerge = dic[@"Data"];
//                            
//                            [arrContainer addObjectsFromArray:arrMerge];
//                            
//                            arrTableData = arrContainer;
                            
                            [tblView reloadData];
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                         [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

#pragma mark - UI Table view Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrTableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"homeScreenOffers";
    CustomCellForTableTableViewCell *cell = [tblView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[CustomCellForTableTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imageOffer];
    }
        
    
    NSURL *imgURL = [NSURL URLWithString:[[arrTableData valueForKey:@"merchant_image"]objectAtIndex:indexPath.row]];
    cell.imageOffer.imageURL = imgURL;
    
    cell.lblOffer.text = [[arrTableData valueForKey:@"title"]objectAtIndex:indexPath.row];
    cell.lblOfferDescription.text = [[arrTableData valueForKey:@"description"]objectAtIndex:indexPath.row];
//    cell.lblOldPrice.text =[NSString stringWithFormat:@"$%@",[[arrTableData valueForKey:@"old_price"]objectAtIndex:indexPath.row]];
    
    NSMutableAttributedString *oldPrice =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"$%@",[[arrTableData valueForKey:@"old_price"]objectAtIndex:indexPath.row]]];
    
    [oldPrice addAttribute:NSStrikethroughStyleAttributeName
                     value:[NSNumber numberWithInt:NSUnderlineStyleSingle]
                     range:NSMakeRange(0, [oldPrice length])];
    
    [cell.lblOldPrice setAttributedText:oldPrice];
    
    cell.lblNewPrice.text =[NSString stringWithFormat:@"$%@",[[arrTableData valueForKey:@"new_price"]objectAtIndex:indexPath.row]];
    cell.lblOfferValidTill.text = [NSString stringWithFormat:@"Valid Till : %@",[[arrTableData valueForKey:@"valid_till"]objectAtIndex:indexPath.row]];
    
   return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    app.flagForFront = NO;
    
    HomeDetailView *HomeDetailView = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDetailView"];
    
    HomeDetailView.couponID = [[arrTableData valueForKey:@"id"]objectAtIndex:indexPath.row];

    [self.navigationController pushViewController:HomeDetailView animated:YES];
    
}

#pragma mark - Location Delegate
// Location Update
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        Lat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        Long = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        NSLog(@"%@",Lat);
        NSLog(@"%@",Long);
    }
    
    // Stop Location Manager
    [locationManager stopUpdatingLocation];
    
    NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        //        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            NSString *address = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
                                 placemark.subThoroughfare, placemark.thoroughfare,
                                 placemark.postalCode, placemark.locality,
                                 placemark.administrativeArea,
                                 placemark.country];
            
            NSLog(@"%@",address);
        } else {
            //            NSLog(@"%@", error.debugDescription);
        }
    } ];
}


- (IBAction)btnMenu:(id)sender {
    
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
 
}

- (IBAction)btnMapView:(id)sender {
    
    [btnLocation setBackgroundImage:[UIImage imageNamed:@"location_icon"] forState:UIControlStateNormal];
    [btnMapView setBackgroundImage:[UIImage imageNamed:@"map_view_select_icon"] forState:UIControlStateNormal];
    
    app.flagForFront = YES;
    
    MapLocations *MapLocations = [self.storyboard instantiateViewControllerWithIdentifier:@"MapLocations"];
    
    MapLocations.arrVal = arrTableData;
     
    [self.navigationController pushViewController:MapLocations animated:YES];
    
}

- (IBAction)btnCurrentLocation:(id)sender {
    
    [btnLocation setBackgroundImage:[UIImage imageNamed:@"location_icon_select"] forState:UIControlStateNormal];
    
    [btnMapView setBackgroundImage:[UIImage imageNamed:@"map_view_icon"] forState:UIControlStateNormal];
    
    usingCurrentLocation = YES;
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(HomeOffers)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    
}

- (IBAction)btnGo:(id)sender {
    
    if ([txtSearch.text isEqualToString:@""]) {
        
        [CommonFunctions AlertWithMsg:@"Please enter search text !!!"];
        
    }else{
    [self.view endEditing:YES];
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(searchOffers)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    }
    
}


@end
