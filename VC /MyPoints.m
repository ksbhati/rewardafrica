//
//  MyPoints.m
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "MyPoints.h"
#import  "MyPointsOffer.h"

@interface MyPoints (){
    
    NSArray *arrTableData ,*filteredArray,*totalArray;
    BOOL isSearching;
    
}

@end

@implementation MyPoints

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
        // Do any additional setup after loading the view.
    
}
-(void)viewWillAppear:(BOOL)animated{
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(myPoints)
                   onTarget:self
                 withObject:nil
                   animated:YES];

}

- (BOOL)textFieldShouldReturn:(UITextField *)aTextField
{
    [aTextField resignFirstResponder];
    return YES;
    
}

-(void)textFieldDidChange:(UITextField*)textField
{
    NSString *substring = [NSString stringWithString:textField.text];
    
    [self searchAutocompleteEntriesWithSubstring:substring];
    
}


- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    if (txtSearch.text.length > 0) {
        isSearching = YES;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"merchant_name contains[cd] %@",
                                        substring];
        
        filteredArray = [totalArray filteredArrayUsingPredicate:resultPredicate];
        arrTableData = [filteredArray mutableCopy];
        [tblView reloadData];
    }else{
        isSearching = NO;
        arrTableData = [totalArray mutableCopy];
        [tblView reloadData];
    }
    
}


#pragma mark - Webservice

- (void)myPoints {
    
    //   http://codetesting.xyz/loyalty-africa/app/get.php?action=my-points&u_id=1
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=my-points&u_id=%@",userID];
    
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            
                            totalArray = dic[@"Data"];
                            arrTableData = [totalArray mutableCopy];
                            filteredArray = [NSMutableArray arrayWithCapacity:[dic[@"Data"] count]];
                            [tblView reloadData];
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}


#pragma mark - UI Table view Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrTableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"MyPoints";
    
    CustomCellForTableTableViewCell *cell = [tblView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[CustomCellForTableTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imageOffer];
    }
    
    [cell.lblMyPointsRedeem setTransform:CGAffineTransformMakeRotation(3.14/2)];
    
    
    cell.lblMyPointsTitle.text = [[arrTableData valueForKey:@"merchant_name"]objectAtIndex:indexPath.row];
    cell.lblMyPointsTotalPoints.text =[NSString stringWithFormat:@"%@ Points",[[arrTableData valueForKey:@"remain_points"]objectAtIndex:indexPath.row]];
    
    NSURL *imgURL = [NSURL URLWithString:[[arrTableData valueForKey:@"merchant_image"]objectAtIndex:indexPath.row]];
    cell.imageMyPointsVendor.imageURL = imgURL;
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyPointsOffer *MyPointsOffer = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPointsOffer"];
    
    MyPointsOffer.merchantID = [[arrTableData valueForKey:@"merchant_id"]objectAtIndex:indexPath.row];
    MyPointsOffer.merchantPoints = [[arrTableData valueForKey:@"user_points"]objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:MyPointsOffer animated:YES];
    
}

#pragma mark - IBAction Buttons

- (IBAction)btnMenu:(id)sender {
    
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
    
}

@end
