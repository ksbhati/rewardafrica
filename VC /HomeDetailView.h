//
//  HomeDetailView.h
//  Africa
//
//  Created by Kunal Singh Bhati on 30/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDetailView : UIViewController<UIAlertViewDelegate>{
    
    IBOutlet UILabel *lblTitleForDetail;
    IBOutlet UIImageView *imageOffer;
    IBOutlet UILabel *lblNewPrice;
    IBOutlet UILabel *lblOldPrice;
    IBOutlet UILabel *lblPoints;
    
    IBOutlet UILabel *lblOfferTitle;
    IBOutlet UITextView *lblDescription;
    IBOutlet UILabel *lblExpDate;

    IBOutlet UILabel *lblRestName;
    IBOutlet UILabel *lblAddLine1;
    IBOutlet UILabel *lblAddLine2;

    IBOutlet UILabel *lblPrice;

}
@property NSString *couponID;

- (IBAction)btnMapView:(id)sender;
- (IBAction)btnMenu:(id)sender;
- (IBAction)btnBuyNow:(id)sender;
- (IBAction)btnPayAtStore:(id)sender;

@end
