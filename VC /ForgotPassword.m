//
//  ForgotPassword.m
//  Africa
//
//  Created by Kunal Singh Bhati on 04/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import "ForgotPassword.h"

@interface ForgotPassword ()

@end

@implementation ForgotPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Webservice
- (void)fogotPassword{
    
    //   http://codetesting.xyz/loyalty-africa/app/forgotpassword.php?email=demo@gmail.com
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"forgotpassword.php?email=%@",txtForgotPAssword.text];
    
    pr(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:[strUrl stringByURLEncode]]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                NSError *error;
                NSDictionary *dic= [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&error];
                pr(@"%@, error %@",dic,error);
                if (dic && !error)
                {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"]) {
                        //// login Successfull ////
                        
                        [CommonFunctions AlertWithMsg:@"Password successfully sent !!!"];
                        [self.navigationController popViewControllerAnimated:YES];
                        
                        
                    } else if ([dic[@"Response"] isEqualToString:@" failure"]) {
                        NSString *message = dic[@"Message"];
                                             
                        [CommonFunctions AlertWithMsg:message];
                        
                    } else {
                        
                        
                    }
                } else {
                    
                    [CommonFunctions showServerNotFoundError];
                }
            }
            else{
                
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}



- (IBAction)btnGetWebService:(id)sender {
    
    if ([txtForgotPAssword.text isEqualToString:@""]) {
        [CommonFunctions AlertWithMsg:@"Please enter Email Address"];
    }else{
        
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(fogotPassword)
                       onTarget:self
                     withObject:nil
                       animated:YES];
}
    
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
