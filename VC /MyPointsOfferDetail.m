//
//  MyPointsOfferDetail.m
//  Africa
//
//  Created by Kunal Singh Bhati on 31/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "MyPointsOfferDetail.h"

@interface MyPointsOfferDetail ()

@end

@implementation MyPointsOfferDetail
@synthesize rewardID,merchantPoints;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(myPointsOfferDetail)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    
}

#pragma mark - Webservice

- (void)myPointsOfferDetail {
    
    //    http://codetesting.xyz/loyalty-africa/app/get.php?action=reward-detail&reward_id=1&u_id=1
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=reward-detail&reward_id=%@&u_id=%@",rewardID,userID];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            
                            lblTilteForCoupon.text = [[dic[@"Data"]valueForKey:@"reward_title"]objectAtIndex:0];
                            lblTitleDetails.text = [[dic[@"Data"]valueForKey:@"reward_title"]objectAtIndex:0];
                            
                            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageOfferDetail];
                            
                            
                            NSURL *imgURL = [NSURL URLWithString:[[dic[@"Data"]valueForKey:@"reward_image"] objectAtIndex:0]];
                            imageOfferDetail.imageURL = imgURL;
                            
                            NSURL *imgURL2 = [NSURL URLWithString:[[dic[@"Data"]valueForKey:@"reward_image"] objectAtIndex:0]];
                            imageOfferDetail.imageURL = imgURL2;
                            
                            lblPointDetail.text  = [[dic[@"Data"]valueForKey:@"reward_point"]objectAtIndex:0];
                            
                            lblDescriptionDetail.text= [[dic[@"Data"]valueForKey:@"reward_description"]objectAtIndex:0];
                            
                            lblValidTillDetail.text    = [NSString stringWithFormat:@"Valid Till : %@",[[dic[@"Data"]valueForKey:@"reward_valid_till"]objectAtIndex:0]];
                            
                            lblMerchantNameDetail.text   = [[dic[@"Data"]valueForKey:@"merchant_name"]objectAtIndex:0];
                            lblMerchantAddressDetail.text   = [[dic[@"Data"]valueForKey:@"merchant_address"]objectAtIndex:0];
                            
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

// Web service for buying cupon using points
- (void)redeemReward {
    
//   http://codetesting.xyz/loyalty-africa/app/reward.php?action=redeem&reward_id=1&u_id=1
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"reward.php?action=redeem&reward_id=%@&u_id=%@",rewardID,userID];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        [CommonFunctions AlertWithMsg:@"Purchase Successfully.You can redeem this coupon from My Rewards.  "];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}
#pragma mark - Alert View Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(redeemReward)
                       onTarget:self
                     withObject:nil
                       animated:YES];
    }
    if (buttonIndex == 1)
    {
        //Code for download button
    }
}


- (IBAction)btnBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnRedeemNow:(id)sender {
    
    NSInteger *merVal = [merchantPoints integerValue];
    NSInteger *offerVal = [lblPointDetail.text integerValue];
    
    if (merVal< offerVal) {
        
        [CommonFunctions AlertWithMsg:@"You dont have enough points for this offer !!!"];
        
    }else{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Pikadily" message:@"Would you like to redeem this Coupon!!!" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    [alert show];
    }
}
@end
