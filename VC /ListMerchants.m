//
//  ListMerchants.m
//  Africa
//
//  Created by Kunal Singh Bhati on 22/02/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import "ListMerchants.h"

@interface ListMerchants (){
    
    NSArray *arrTableData ,*filteredArray,*totalArray;
    BOOL isSearching;
}

@end

@implementation ListMerchants
@synthesize userName,password,name,emailAddress,age,postalCode;


- (void)viewDidLoad {
    [super viewDidLoad];
     [txtSearchList addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    if ([userDict[@"circle_of_friends"]isKindOfClass:[NSArray class]]) {
        app.arrMerchantIds = [[[userDict valueForKey:@"circle_of_friends"]valueForKey:@"id"] mutableCopy];
    }
    
    // Do any additional setup after loading the view.
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(listMerchants)
                   onTarget:self
                 withObject:nil
                   animated:YES];
}

#pragma mark - textField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)aTextField
{
    [aTextField resignFirstResponder];
    return YES;
    
}

-(void)textFieldDidChange:(UITextField*)textField
{
    NSString *substring = [NSString stringWithString:textField.text];
    
    [self searchAutocompleteEntriesWithSubstring:substring];
}


- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    if (txtSearchList.text.length > 0) {
        
        isSearching = YES;
        
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"username contains[cd] %@",
                                        substring];
        
        filteredArray = [totalArray filteredArrayUsingPredicate:resultPredicate];
        arrTableData = [filteredArray mutableCopy];
        [tblView reloadData];
        
    }else{
        
        isSearching = NO;
        arrTableData = [totalArray mutableCopy];
        [tblView reloadData];
        
    }
    
}

#pragma mark - Webservice

- (void)listMerchants {
    
//    http://codetesting.xyz/loyalty-africa/app/get.php?action=merchant-list
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=merchant-list"];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            totalArray = dic[@"Data"];
                            arrTableData = [totalArray mutableCopy];
                            filteredArray = [NSMutableArray arrayWithCapacity:[dic[@"Data"] count]];
                            [tblView reloadData];
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}
-(void)registerUser1{
    
    // http://codetesting.xyz/loyalty-africa/app/app_register.php?username=ikdemo&email=ikdemo@gmail.com&category=1,2&password=1234&age=18&postal_code=302019&logintyp=1&circle_of_friends=1,2,3,4,5,6,7,8,9
    
    NSString *assignedJoinesComponents = [app.arrMerchantIds componentsJoinedByString:@","];
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"app_register.php?logintyp=1&username=%@&email=%@&category=&password=%@&age=%@&postal_code=%@&circle_of_friends=%@",userName,emailAddress,password,age,postalCode,assignedJoinesComponents];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"true"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            [CommonFunctions AlertWithMsg:@"Registeration done successfully!!! Please check your email to Activate you account"];
                            
                            app.isRegistered = YES;
                            app.arrMerchantIds = [NSMutableArray new];
                            
                            [self.navigationController popToRootViewControllerAnimated:YES];
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"false"])
                    {
                        [CommonFunctions AlertTitle:@"Offers" withMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertTitle:@"Offers" withMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                //                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}
#pragma mark - Webservice
-(void)saveCircleOfFriends {
    
    //http://codetesting.xyz/loyalty-africa/app_update.php?for=app_user&id=1&username=demo&email=demo@gmail.com&password=admin&name=demoname&contact_no=1234567890&address_line_1=karkwal&address_line_2=mertacity&city=ajmer&state=bihar&country=indonesia&postal_code=123456&device_type=1&device_id=121212&category=1,2,3,4&role=2&age=21
    
    NSString *assignedJoinesComponents = [app.arrMerchantIds componentsJoinedByString:@","];

    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"app_update.php?for=app_user&id=%@&username=&email=&password=&name=&contact_no=&address_line_1=&address_line_2=&city=&state=&country=&postal_code=&device_type=1&device_id=&category=&role=&age=&circle_of_friends=%@",userID,assignedJoinesComponents];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"true"])
                    {
                        app.fromProfileScreen = NO;
                        userDict = dic[@"Data"];
                        
                        [CommonFunctions userLoginDetail:dic[@"Data"]];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [self dismissViewControllerAnimated:NO completion:nil];
                    
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"faliure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}



#pragma mark - UI Table view Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrTableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"merchantList";
    CustomCellForTableTableViewCell *cell = [tblView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[CustomCellForTableTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imageViewMerchantList];
    }
    
    cell.imageChecked.image = [UIImage imageNamed:@"no_image_icon"];
    
    NSURL *imgURL = [NSURL URLWithString:[[arrTableData valueForKey:@"image"]objectAtIndex:indexPath.row]];
    cell.imageViewMerchantList.imageURL = imgURL;
    cell.lblMerchantNameList.text = [[arrTableData valueForKey:@"username"]objectAtIndex:indexPath.row];
    
    if ([app.arrMerchantIds containsObject:[arrTableData[indexPath.row]valueForKey:@"id"]]) {
        cell.imageChecked.image = [UIImage imageNamed:@"select_circle_active.png"];
    }
    else{
        cell.imageChecked.image = [UIImage imageNamed:@"select_circle.png"];
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (![app.arrMerchantIds containsObject:[arrTableData[indexPath.row]valueForKey:@"id"]]){
        
        [app.arrMerchantIds addObject:[arrTableData[indexPath.row]valueForKey:@"id"]];
        
    }else{
        
        [app.arrMerchantIds removeObject:[arrTableData[indexPath.row]valueForKey:@"id"]];
    }
    
    [tblView reloadData];

    
}

- (IBAction)btnClose:(id)sender {
    
    app.isRegistered = NO;
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)btnOk:(id)sender {
    app.isRegistered = YES;
    
    if (app.fromProfileScreen) {
        
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(saveCircleOfFriends)
                       onTarget:self
                     withObject:nil
                       animated:YES];

    }else{
        
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(registerUser1)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    }
    
}
@end
