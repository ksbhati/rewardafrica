//
//  HomeDetailView.m
//  Africa
//
//  Created by Kunal Singh Bhati on 30/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "HomeDetailView.h"
#import "buyCoupon.h"

@interface HomeDetailView (){
    
    NSArray *arrTransfer;
    NSString *priceToTransfer;
}

@end

@implementation HomeDetailView
@synthesize couponID;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(homeDetail)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    
}

#pragma mark - Webservice

- (void)homeDetail {
    
    //    http://codetesting.xyz/loyalty-africa/app/get.php?action=coupon-detail&coupon_id=2&u_id=15
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=coupon-detail&coupon_id=%@&u_id=%@",couponID,userID];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            arrTransfer = dic[@"Data"];
                            
                            lblTitleForDetail.text = [[dic[@"Data"]valueForKey:@"title"]objectAtIndex:0];
                            
                            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageOffer];
                            
                            NSLog(@"%@",[[dic[@"Data"]valueForKey:@"coupon_image"] objectAtIndex:0]);
                            
                            NSURL *imgURL = [NSURL URLWithString:[[dic[@"Data"]valueForKey:@"coupon_image"] objectAtIndex:0]];
                            imageOffer.imageURL = imgURL;
                            
                            priceToTransfer = [[dic[@"Data"]valueForKey:@"new_price"]objectAtIndex:0];
                            
                            lblNewPrice.text=[NSString stringWithFormat:@"$%@",[[dic[@"Data"]valueForKey:@"new_price"]objectAtIndex:0]];
                            lblOldPrice.text=[NSString stringWithFormat:@"$%@",[[dic[@"Data"]valueForKey:@"old_price"]objectAtIndex:0]];
                            
                            NSMutableAttributedString *oldPrice =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"$%@",[[dic[@"Data"]valueForKey:@"old_price"]objectAtIndex:0]]];
                            
                            [oldPrice addAttribute:NSStrikethroughStyleAttributeName
                                             value:[NSNumber numberWithInt:NSUnderlineStyleSingle]
                                             range:NSMakeRange(0, [oldPrice length])];
                            
                            [lblOldPrice setAttributedText:oldPrice];
                            
                            lblPoints.text  = [[dic[@"Data"]valueForKey:@"bonus_point"]objectAtIndex:0];
                            
                            lblOfferTitle.text = [[dic[@"Data"]valueForKey:@"title"]objectAtIndex:0];
                            lblDescription.text= [[dic[@"Data"]valueForKey:@"description"]objectAtIndex:0];
                            lblExpDate.text    = [NSString stringWithFormat:@"Valid Till : %@",[[dic[@"Data"]valueForKey:@"valid_till"]objectAtIndex:0]];
                            
                            lblRestName.text   = [[dic[@"Data"]valueForKey:@"merchant_name"]objectAtIndex:0];
                            lblAddLine1.text   = [[dic[@"Data"]valueForKey:@"merchant_address"]objectAtIndex:0];
                            
                            lblPrice.text      = [NSString stringWithFormat:@"Price :$%@",[[dic[@"Data"]valueForKey:@"new_price"]objectAtIndex:0]];
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

// Web service for not buying coupons
- (void)buyCoupon {
    
    //http://codetesting.xyz/loyalty-africa/app/coupon.php?action=use&coupon_id=3&u_id=1&coupon_redeem_type=buy_now
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"coupon.php?action=use&coupon_id=%@&u_id=%@&coupon_redeem_type=buy_now",couponID,userID];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        [CommonFunctions AlertWithMsg:@"Purchase Successfully.You can redeem this coupon from My Coupon."];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

// Web service for not buying coupons
- (void)payAtStore {
    
    //    http://codetesting.xyz/loyalty-africa/app/coupon.php?action=use&coupon_id=3&u_id=1&coupon_redeem_type=pay_at_store
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"coupon.php?action=use&coupon_id=%@&u_id=%@&coupon_redeem_type=pay_at_store",couponID,userID];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        [CommonFunctions AlertWithMsg:@"Purchase Successfully.You can redeem this coupon from My Coupon.You have to pay the amount of coupon at the time of purchase !!!"];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

#pragma mark - Alert View Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        buyCoupon *buyCoupon = [self.storyboard instantiateViewControllerWithIdentifier:@"buyCoupon"];
        
        buyCoupon.couponID = couponID;
        buyCoupon.buyAmount= priceToTransfer;
        
        [self.navigationController pushViewController:buyCoupon animated:YES];
        
    }
    if (buttonIndex == 1)
    {
        //Code for download button
    }
}

#pragma mark - IBAction

- (IBAction)btnMapView:(id)sender {
    
    if(app.flagForFront){
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        
        MapLocations *MapLocations = [self.storyboard instantiateViewControllerWithIdentifier:@"MapLocations"];
        
        MapLocations.arrVal = [arrTransfer mutableCopy];
        MapLocations.fromDetailPage = YES;
        
        [self.navigationController pushViewController:MapLocations animated:YES];
        
    }
    
}

- (IBAction)btnMenu:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btnBuyNow:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Pikadily" message:@"You want to buy this Coupon!!!" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    [alert show];
    
    
}

- (IBAction)btnPayAtStore:(id)sender {
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(payAtStore)
                   onTarget:self
                 withObject:nil
                   animated:YES];
}

@end
