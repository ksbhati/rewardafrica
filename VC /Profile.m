//
//  Profile.m
//  Africa
//
//  Created by Kunal Singh Bhati on 31/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "Profile.h"
#import "Preferences.h"
#import "ListMerchants.h"


@interface Profile ()<UIImagePickerControllerDelegate>{
    UIScrollView *prefView;
    NSMutableArray *arrCat;
}

@end

@implementation Profile

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrCat = [NSMutableArray new];
    
    if ([userDict[@"user_preferences"]isKindOfClass:[NSArray class]]) {
        arrCat = [[[userDict valueForKey:@"user_preferences"]valueForKey:@"id"] mutableCopy];
    }

    CGSize scrollViewContentSize = CGSizeMake(320, 900);
    [scrollView setContentSize:scrollViewContentSize];
    
    imgView.layer.cornerRadius = imgView.frame.size.width / 2;
    imgView.clipsToBounds = true;
    
    viewEnterName.hidden = YES;
    
    NSURL *imgURL = [NSURL URLWithString:userDict[@"image"]];
    imgView.imageURL = imgURL;

}

#pragma mark - textField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    lblName.text = userDict[@"name"];
    txtEditName.text = userDict[@"name"];
    lblUserName.text = userDict [@"username"];
    
    NSArray *labToRemove = [scrollView subviews];
    
    for (UILabel *v in labToRemove)
    {
        if (v.tag >100) {
            [v removeFromSuperview];
        }
        
    }
    for (UIImageView *k in labToRemove)
    {
        if (k.tag >300) {
            [k removeFromSuperview];
        }
        
    }

    
    NSArray* foo = [userDict valueForKey:@"user_preferences"]  ;
   
    for (int i = 0; i < foo.count; i++)
    {
        int lbly = 310;
        int imagly = 280;
        
        lbly = lbly + 25*(i+1);
        
        UILabel *label =  [[UILabel alloc] initWithFrame: CGRectMake(40,lbly,300,20)];
        label.text = [foo[i]valueForKey:@"name"];
        label.tag = 101+i;
        [label setFont:[UIFont fontWithName:@"Avenir-Book" size:13]];
        [scrollView addSubview:label];
        
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(10, lbly, 20, 20)];
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:image];
        NSURL *imgURL = [NSURL URLWithString:[foo[i]valueForKey:@"img"]];
        image.imageURL = imgURL;
        image.tag = 301+i;
//        [image setContentMode:UIViewContentModeCenter];
        [scrollView addSubview:image];
        
    }


}

// Add business Webservice
- (void) sendImageToServer {
    
    if(imgView.image != nil)
    {
        
//    http://codetesting.xyz/loyalty-africa/app//app_update.php?for=app_user&id=1&username=demo&email=demo@gmail.com&password=admin&name=demoname&contact_no=1234567890&address_line_1=karkwal&address_line_2=mertacity&city=ajmer&state=bihar&country=indonesia&postal_code=123456&device_type=1&device_id=121212&category=1,2,3,4&role=2&age=21
        
        NSString *commonString;
        NSArray *items = [arrCat mutableCopy];
        commonString = [items componentsJoinedByString:@","];
        
       NSString *str=[SiteAPIURL stringByAppendingFormat:@"app_update.php?for=app_user&id=%@&username=%@&email=%@&password=%@&name=%@&contact_no=&address_line_1=&address_line_2=&city=&state=&country=&postal_code=&device_type=1&device_id=&category=%@&role=&age=",userID,userDict[@"username"],userDict[@"email"],lblNewPassword.text,lblName.text,commonString];
        
        NSString *newString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString *urlString = [NSString stringWithFormat:@"%@",newString];
        
        
        NSURL *siteURL = [[NSURL alloc] initWithString:urlString];
        
        // create the connection
        NSMutableURLRequest *siteRequest = [NSMutableURLRequest requestWithURL:siteURL
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:30.0];
        
        // change type to POST (default is GET)
        [siteRequest setHTTPMethod:@"POST"];
        
        // just some random text that will never occur in the body
        NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        
        // header value
        NSString *headerBoundary = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",
                                    stringBoundary];
        
        // set header
        [siteRequest addValue:headerBoundary forHTTPHeaderField:@"Content-Type"];
        
        //add body
        NSMutableData *postBody = [NSMutableData data];
        //    pro(@"body made");
        
        //image
        [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@\"\r\n",@"iphone.png"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postBody appendData:[@"Content-Type: image/jpg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *imageData =UIImageJPEGRepresentation(imgView.image, 0.1);
        
        [postBody appendData:imageData];
        
        [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        // final boundary
        [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        //pr(@"message data post data %@",postBody);
        
        // add body to post
        [siteRequest setHTTPBody:postBody];
        
        NSURLResponse *response;
        NSError *error;
        NSData *returnData = [NSURLConnection sendSynchronousRequest:siteRequest
                                                   returningResponse:&response
                                                               error:&error];
        
        //        NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:nil];
        
        NSLog(@"%@",dict);
        
        if ([dict[@"Response"] isEqualToString:@"true"]) {
            
            [CommonFunctions AlertWithMsg:@"User info updated successfully"];
            
            userDict = dict[@"Data"];
            
            [CommonFunctions userLoginDetail:dict[@"Data"]];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"bindMenuTitle" object:nil];
            
            
                                      
        }else{
            
        }
        
    }
}

#pragma mark - IU Picker view control delegates
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    //Or you can get the image url from AssetsLibrary
    NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    imgView.image = image;
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSData *imageData = UIImagePNGRepresentation(image);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",@"cached"]];
        
        NSLog((@"pre writing to file"));
        if (![imageData writeToFile:imagePath atomically:NO])
        {
            NSLog((@"Failed to cache image data to disk"));
        }
        else
        {
            //            NSLog((@"the cachedImagedPath is %@",imagePath));
        }
        
    }];
    
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - IBAction buttons

- (IBAction)btnGotoPreferences:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    Preferences *Preferences =
    [storyboard instantiateViewControllerWithIdentifier:@"Preferences"];
    
    
    [self presentViewController:Preferences
                       animated:YES
                     completion:nil];
}

- (IBAction)btnMenu:(id)sender {
    
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
}

- (IBAction)btnEditName:(id)sender {
    
    viewEnterName.hidden = NO;
}

- (IBAction)btnUploadImage:(id)sender {
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];

}

- (IBAction)btnUpdateProfile:(id)sender {
    
    [self.view endEditing:YES];
    
        if ([lblNewPassword.text isEqualToString:lblRePassword.text]) {
            
            [mainWindow addSubview:HUD];
            [HUD showWhileExecuting:@selector(sendImageToServer)
                           onTarget:self
                         withObject:nil
                           animated:YES];
            
        }else{
            
            [CommonFunctions AlertWithMsg:@"Password does'nt match"];
            
        }
}


- (IBAction)btnOK:(id)sender {
    lblName.text = txtEditName.text;
    viewEnterName.hidden = YES;
    [self.view endEditing:YES];
}


- (IBAction)btnCancel:(id)sender {
    viewEnterName.hidden = YES;
    [self.view endEditing:YES];
}

- (IBAction)btnSetCircleOfFriends:(id)sender {
    app.fromProfileScreen = YES;
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    ListMerchants *ListMerchants =
    [storyboard instantiateViewControllerWithIdentifier:@"ListMerchants"];
    
    ListMerchants.userName = @"";
    ListMerchants.password = @"";
    ListMerchants.emailAddress = @"";
    ListMerchants.postalCode = @"";
    ListMerchants.age = @"";
    
    [self presentViewController:ListMerchants
                       animated:YES
                     completion:nil];

    
}


@end
