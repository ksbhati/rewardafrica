//
//  Preferences.m
//  Africa
//
//  Created by Kunal Singh Bhati on 05/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import "Preferences.h"
#import "CustomColllectionCell.h"

@interface Preferences (){
    
    NSMutableArray *arrTableData,*arrCategories,*arrIndexpath;
    
}

@end

@implementation Preferences

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrCategories = [NSMutableArray new];
    arrIndexpath = [NSMutableArray new];
    
    if ([userDict[@"user_preferences"]isKindOfClass:[NSArray class]]) {
    arrCategories = [[[userDict valueForKey:@"user_preferences"]valueForKey:@"id"] mutableCopy];
    }
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(preferences)
                   onTarget:self
                 withObject:nil
                   animated:YES];
}


#pragma mark - Webservice
-(void)savePreferences {
    
    //http://codetesting.xyz/loyalty-africa/app_update.php?for=app_user&id=1&username=demo&email=demo@gmail.com&password=admin&name=demoname&contact_no=1234567890&address_line_1=karkwal&address_line_2=mertacity&city=ajmer&state=bihar&country=indonesia&postal_code=123456&device_type=1&device_id=121212&category=1,2,3,4&role=2&age=21
    
    NSString *commonString;
    NSArray *items = [arrCategories mutableCopy];
    commonString = [items componentsJoinedByString:@","];
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"app_update.php?for=app_user&id=%@&username=&email=&password=&name=&contact_no=&address_line_1=&address_line_2=&city=&state=&country=&postal_code=&device_type=1&device_id=&category=%@&role=&age=&circle_of_friends=",userID,commonString];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"true"])
                    {
                        userDict = dic[@"Data"];
                        
                        [CommonFunctions userLoginDetail:dic[@"Data"]];
                        
                        userDict = dic[@"Data"];
                        
                        [CommonFunctions userLoginDetail:dic[@"Data"]];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];

                        
                        if ([app.fromLogin isEqualToString:@"TRUE"]) {
                            
                            app.fromLogin = @"FALSE";
                            
                            [self.navigationController popViewControllerAnimated:YES];
                            
                        }else{
                            
                            [self dismissViewControllerAnimated:NO completion:nil];
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"faliure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}


-(void)preferences {
    
    //http://codetesting.xyz/loyalty-africa/app/get.php?action=category-list
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=category-list"];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            arrTableData = [NSMutableArray new];
                            arrTableData = dic[@"Data"];
                            
                            [collView reloadData];
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}


#pragma mark - Collection view Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [arrTableData count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"preferences";
    
    CustomColllectionCell *cell = (CustomColllectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.lblTitles.text = [[arrTableData valueForKey:@"name"] objectAtIndex:indexPath.row];
    NSURL *imgURL = [NSURL URLWithString:[[arrTableData valueForKey:@"image"] objectAtIndex:indexPath.row]];
    cell.lblImageView.imageURL = imgURL;
    
    cell.imgDone.hidden = YES;
    
//    if ([arrIndexpath containsObject:indexPath]) {
//        
//        cell.imgDone.hidden = NO;
//    }
    
    if ([arrCategories containsObject:[[arrTableData valueForKey:@"id"] objectAtIndex:indexPath.row]]) {
        
        cell.imgDone.hidden = NO;
    }
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([arrCategories containsObject:[[arrTableData valueForKey:@"id"] objectAtIndex:indexPath.row]]) {
        
        // do something
//        [arrIndexpath removeObject:indexPath];
        [arrCategories removeObject:[[arrTableData objectAtIndex:indexPath.row] valueForKey:@"id"]];
        
    }else{
        
//        [arrIndexpath addObject:indexPath];
        [arrCategories addObject:[[arrTableData objectAtIndex:indexPath.row] valueForKey:@"id"]];
        
    }
    
//    [collView reloadItemsAtIndexPaths:arrCategories];
    
    [collView reloadData];
    
}


- (IBAction)btnClose:(id)sender {
    
    if ([app.fromLogin isEqualToString:@"TRUE"]) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }else{
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }

}


- (IBAction)btnOk:(id)sender {
    
    if ([arrCategories count]< 3) {
        
        [CommonFunctions AlertWithMsg:@"Please select at least 3 categories !!!"];
        
    }else{
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(savePreferences)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    }
    
}


@end
