//
//  MyCoupons.m
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "MyCoupons.h"
#import "MyCouponsDetail.h"

@interface MyCoupons (){
    
    NSArray *arrTableData ,*filteredArray,*totalArray;
   
    BOOL isSearching;
}

@end

@implementation MyCoupons

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(myCoupons)
                   onTarget:self
                 withObject:nil
                   animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)aTextField
{
    [aTextField resignFirstResponder];
    return YES;
    
}

-(void)textFieldDidChange:(UITextField*)textField
{
    NSString *substring = [NSString stringWithString:textField.text];
    
    [self searchAutocompleteEntriesWithSubstring:substring];
    
}


- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    if (txtSearch.text.length > 0) {
        isSearching = YES;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"coupon_title contains[cd] %@",
                                        substring];
        
        filteredArray = [totalArray filteredArrayUsingPredicate:resultPredicate];
        arrTableData = [filteredArray mutableCopy];
        [tblView reloadData];
    }else{
        isSearching = NO;
        arrTableData = [totalArray mutableCopy];
        [tblView reloadData];
    }
    
}

#pragma mark - Webservice

- (void)myCoupons {
    
   // http://codetesting.xyz/loyalty-africa/app/get.php?action=my-coupons&u_id=4
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=my-coupons&u_id=%@",userID];
    
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            totalArray = dic[@"Data"];
                            arrTableData = [totalArray mutableCopy];
                            filteredArray = [NSMutableArray arrayWithCapacity:[dic[@"Data"] count]];
                            [tblView reloadData];
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                       [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}


#pragma mark - UI Table view Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrTableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"MyCupons";
    CustomCellForTableTableViewCell *cell = [tblView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[CustomCellForTableTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imageOffer];
    }
    
    cell.lblOffer.text = [[arrTableData valueForKey:@"coupon_title"]objectAtIndex:indexPath.row];
    cell.lblOfferDescription.text = [[arrTableData valueForKey:@"coupon_description"]objectAtIndex:indexPath.row];
    cell.lblOldPrice.text =[NSString stringWithFormat:@"$%@",[[arrTableData valueForKey:@"coupon_old_price"]objectAtIndex:indexPath.row]];
    cell.lblNewPrice.text =[NSString stringWithFormat:@"$%@",[[arrTableData valueForKey:@"coupon_new_price"]objectAtIndex:indexPath.row]];
    
    NSURL *imgURL = [NSURL URLWithString:[[arrTableData valueForKey:@"merchant_image"]objectAtIndex:indexPath.row]];
    cell.imageOffer.imageURL = imgURL;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    app.flagForFront = NO;
    MyCouponsDetail *MyCouponsDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"MyCouponsDetail"];
    
    MyCouponsDetail.couponIDForMyCouponDetail = [[arrTableData valueForKey:@"coupon_id"]objectAtIndex:indexPath.row];

    [self.navigationController pushViewController:MyCouponsDetail animated:YES];
}


- (IBAction)btnMenu:(id)sender {
    
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
    
}

- (IBAction)btnMapView:(id)sender {
    
    app.flagForFront = YES;
    
    MapLocations *MapLocations = [self.storyboard instantiateViewControllerWithIdentifier:@"MapLocations"];
    
    MapLocations.arrVal = [arrTableData mutableCopy];
    MapLocations.fromDetailPage = NO;
    MapLocations.page = @"MyCoupon";
    
    [self.navigationController pushViewController:MapLocations animated:YES];
    
}


@end
