//
//  MapLocations.h
//  Africa
//
//  Created by Kunal Singh Bhati on 05/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapLocations : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>{
    
    IBOutlet MKMapView *mapView;

}

@property NSMutableArray *arrVal;
@property NSString *page;

@property BOOL fromDetailPage;

- (IBAction)btnClose:(id)sender;


@end
