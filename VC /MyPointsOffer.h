//
//  MyPointsOffer.h
//  Africa
//
//  Created by Kunal Singh Bhati on 31/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPointsOffer : UIViewController{
    
    IBOutlet UITableView *tblView;
    IBOutlet UITextField *txtSearch;
    
}

@property NSString *merchantID;
@property NSString *merchantPoints;

- (IBAction)btnBack:(id)sender;

@end
