//
//  MyPoints.h
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPoints : UIViewController{
    
    IBOutlet UITextField *txtSearch;
    
    IBOutlet UITableView *tblView;
}
- (IBAction)btnMenu:(id)sender;

@end
