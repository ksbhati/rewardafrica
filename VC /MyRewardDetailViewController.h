//
//  MyRewardDetailViewController.h
//  Africa
//
//  Created by Kunal Singh Bhati on 08/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRewardDetailViewController : UIViewController{
    
    
    IBOutlet UILabel *lblTilteForCoupon;
    IBOutlet UIImageView *imageOfferDetail;
    IBOutlet UILabel *lblPointDetail;
    IBOutlet UILabel *lblTitleDetails;
    IBOutlet UITextView *lblDescriptionDetail;
    IBOutlet UILabel *lblValidTillDetail;
    IBOutlet UILabel *lblMerchantNameDetail;
    IBOutlet UILabel *lblMerchantAddressDetail;
    
    IBOutlet UIImageView *btnImageBarcode;
    
}
@property NSString *rewardID;
- (IBAction)btnBack:(id)sender;

@end
