//
//  MapLocations.m
//  Africa
//
//  Created by Kunal Singh Bhati on 05/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import "MapLocations.h"
#import "HomeDetailView.h"
#import "JPSThumbnailAnnotation.h"



#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface MapLocations (){
    
    CLLocationManager *locationManager;
    CLLocation        *currentLocation;
    int count;
    NSMutableArray *annotations;
}

@end

@implementation MapLocations
@synthesize arrVal,fromDetailPage,page;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Map View
   
    mapView.delegate = self;
    annotations = [NSMutableArray new];
    [mapView setZoomEnabled:YES];
    
    
    
    [mapView addAnnotations:[self annotations]];
}

- (NSArray *)annotations {
    
        for (int valcount=0;valcount<[arrVal count];valcount++)
        {
            if(![[[arrVal objectAtIndex:valcount] objectForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
    
                CLLocationCoordinate2D zoomLocation;
                zoomLocation.latitude = [[[arrVal objectAtIndex:valcount] objectForKey:@"latitude"] doubleValue];
              //  NSLog(@"%f",zoomLocation.latitude);
                zoomLocation.longitude =[[[arrVal objectAtIndex:valcount] objectForKey:@"longitude"] doubleValue];
              //  NSLog(@"%f",zoomLocation.longitude);
                
                if ([arrVal count]==1) {
                    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 100000, 100000);
                    MKCoordinateRegion adjustedRegion = [mapView regionThatFits:viewRegion];
                    [mapView setRegion:adjustedRegion animated:YES];
                    mapView.showsUserLocation = YES;
                }
               
               
                if(zoomLocation.latitude != 0 && zoomLocation.longitude != 0)
                {
                    // Empire State Building
                    JPSThumbnail *empire = [[JPSThumbnail alloc] init];
                    NSURL *imageURL = [NSURL URLWithString:[[arrVal objectAtIndex:valcount] objectForKey:@"merchant_image"]];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    empire.image = [UIImage imageWithData:imageData];
                    empire.title = [[arrVal objectAtIndex:valcount] objectForKey:@"coupon_title"];
                    empire.subtitle = [[arrVal objectAtIndex:valcount] objectForKey:@"merchant_name"];
                    empire.coordinate = CLLocationCoordinate2DMake(zoomLocation.latitude, zoomLocation.longitude);
                    empire.disclosureBlock = ^{
                        
                        if ([page isEqualToString:@"MyCoupon"]) {
                            
                            [self openDetail:[[arrVal objectAtIndex:valcount] objectForKey:@"coupon_id"]];
                        }else{
                            [self openDetail:[[arrVal objectAtIndex:valcount] objectForKey:@"id"]];
                        }
                    
                        
                        
                    };
    
                    [annotations addObject:[JPSThumbnailAnnotation annotationWithThumbnail:empire]];
                }
            }
        }

    
    return [annotations mutableCopy];
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView2 didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didSelectAnnotationViewInMap:mapView];
    }
}

- (void)mapView:(MKMapView *)mapView2 didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didDeselectAnnotationViewInMap:mapView];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView2 viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation conformsToProtocol:@protocol(JPSThumbnailAnnotationProtocol)]) {
        return [((NSObject<JPSThumbnailAnnotationProtocol> *)annotation) annotationViewInMap:mapView];
    }
    return nil;
}

// Annotation clicked

-(void)openDetail:(NSString*)idForCoupon {
    
    if (fromDetailPage) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        
    NSLog(@"coupon id %@",idForCoupon);
    
    HomeDetailView *HomeDetailView = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDetailView"];
    
    HomeDetailView.couponID = idForCoupon;
    
    [self.navigationController pushViewController:HomeDetailView animated:YES];
        
    }
}

- (IBAction)btnClose:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
