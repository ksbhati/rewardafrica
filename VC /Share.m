//
//  Share.m
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import "Share.h"

@interface Share ()

@end

@implementation Share

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnMenu:(id)sender {
    
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
    
}

@end
