//
//  MyPointsOfferDetail.h
//  Africa
//
//  Created by Kunal Singh Bhati on 31/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPointsOfferDetail : UIViewController<UIAlertViewDelegate>{
    
    IBOutlet UILabel *lblTilteForCoupon;
    IBOutlet UIButton *btnRedeemNow;
    
    IBOutlet UIImageView *imageOfferDetail;
    IBOutlet UILabel *lblPointDetail;
    IBOutlet UILabel *lblTitleDetails;
    IBOutlet UITextView *lblDescriptionDetail;
    IBOutlet UILabel *lblValidTillDetail;
    IBOutlet UILabel *lblMerchantNameDetail;
    IBOutlet UILabel *lblMerchantAddressDetail;

}

@property NSString *rewardID;
@property NSString *merchantPoints;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnRedeemNow:(id)sender;









@end
