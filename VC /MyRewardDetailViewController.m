//
//  MyRewardDetailViewController.m
//  Africa
//
//  Created by Kunal Singh Bhati on 08/01/16.
//  Copyright © 2016 Kunal Singh Bhati. All rights reserved.
//

#import "MyRewardDetailViewController.h"

@interface MyRewardDetailViewController ()

@end

@implementation MyRewardDetailViewController
@synthesize rewardID;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(myPointsOfferDetail)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    
}

#pragma mark - Webservice
- (void)myPointsOfferDetail {
    
    //    http://codetesting.xyz/loyalty-africa/app/get.php?action=reward-detail&reward_id=1&u_id=1
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"get.php?action=reward-detail&reward_id=%@&u_id=%@",rewardID,userID];
    
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Response"] isEqualToString:@"success"])
                    {
                        if(![dic[@"Data"]  isEqual:[NSNull null]])
                        {
                            
                            lblTilteForCoupon.text = [[dic[@"Data"]valueForKey:@"reward_title"]objectAtIndex:0];
                            lblTitleDetails.text = [[dic[@"Data"]valueForKey:@"reward_title"]objectAtIndex:0];

                            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageOfferDetail];
                             [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:btnImageBarcode];
                            
                            NSURL *imgURL = [NSURL URLWithString:[[dic[@"Data"]valueForKey:@"reward_image"] objectAtIndex:0]];
                            imageOfferDetail.imageURL = imgURL;
                            
                            NSURL *imgURL2 = [NSURL URLWithString:[[dic[@"Data"]valueForKey:@"reward_qr_code_image"] objectAtIndex:0]];
                            btnImageBarcode.imageURL = imgURL2;
                            
                            
                            lblPointDetail.text  = [[dic[@"Data"]valueForKey:@"reward_point"]objectAtIndex:0];
                            
                            lblDescriptionDetail.text= [[dic[@"Data"]valueForKey:@"reward_description"]objectAtIndex:0];
                            
                            lblValidTillDetail.text    = [NSString stringWithFormat:@"Valid Till : %@",[[dic[@"Data"]valueForKey:@"reward_valid_till"]objectAtIndex:0]];
                            
                            lblMerchantNameDetail.text   = [[dic[@"Data"]valueForKey:@"merchant_name"]objectAtIndex:0];
                            lblMerchantAddressDetail.text   = [[dic[@"Data"]valueForKey:@"merchant_address"]objectAtIndex:0];
                            
                            
                        }
                        
                    }
                    else if ([dic[@"Response"] isEqualToString:@"failure"])
                    {
                        [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                    else {
                       [CommonFunctions AlertWithMsg:dic[@"Message"]];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}



- (IBAction)btnBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
