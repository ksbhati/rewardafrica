//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "Profile.h"



@implementation LeftMenuViewController

#pragma mark - UIViewController Methods -


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.separatorColor = [UIColor lightGrayColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(BindMenuImagewithtitle) name:@"bindMenuTitle" object:nil];
    [self BindMenuImagewithtitle];
}

-(void)BindMenuImagewithtitle{
    
     NSArray *labToRemove = [self.view subviews];
    
    for (UIImageView *v in labToRemove)
    {
        if (v.tag >100) {
            [v removeFromSuperview];
        }
        
    }
    for (UILabel *d in labToRemove)
    {
        if (d.tag >300) {
            [d removeFromSuperview];
        }
        
    }

    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(6, 20, 85, 85)];
    imgView.tag = 101;
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imgView];
    NSURL *imgURL1 = [NSURL URLWithString:userDict[@"image"]];
    imgView.imageURL = imgURL1;
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview: imgView];
    imgView.layer.cornerRadius = imgView.frame.size.width / 2;
    imgView.clipsToBounds = true;
    
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(96, 34, 204, 30)];//Set frame of label in your viewcontroller.
    [label setText:[userDict valueForKey:@"name"]]; //Set text in label.
    label.tag = 301;
    label.font = [UIFont fontWithName:@"AvenirNext-Medium" size: 20.0];
    [label setTextColor:[UIColor whiteColor]]; //Set text color in label.
    [label setTextAlignment:NSTextAlignmentLeft]; //Set text alignment in label.
    [label setClipsToBounds:YES]; //Set its to YES for Corner radius to work.
    [self.view addSubview:label]; //Add it to the view of your choice.
    
}


#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"leftMenuCell"];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
	
	switch (indexPath.row)
	{
		case 0:
            cell.imageView.image = [UIImage imageNamed:@"home_deselect_icon"];
			cell.textLabel.text = @"Home";
            break;
			
		case 1:
            cell.imageView.image = [UIImage imageNamed:@"my_coupons_deselect_icon"];
			cell.textLabel.text = @"My Coupons";
			break;
			
		case 2:
            cell.imageView.image = [UIImage imageNamed:@"my_points_deselect_icon"];
			cell.textLabel.text = @"My Points";
			break;
			
		case 3:
            cell.imageView.image = [UIImage imageNamed:@"my_rewards_deselect_icon"];
			cell.textLabel.text = @"My Rewards";
			break;
        case 4:
            cell.imageView.image = [UIImage imageNamed:@"share_deselect_icon"];
            cell.textLabel.text = @"Share";
            break;
        case 5:
            cell.imageView.image = [UIImage imageNamed:@"terms_and_conditions_deselect_icon"];
            cell.textLabel.text = @"Terms & Conditions";
            break;
    }
	
	cell.backgroundColor = [UIColor clearColor];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"leftMenuCell"];
	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
															 bundle: nil];
	
	UIViewController *vc ;
	
	switch (indexPath.row)
	{
		case 0:
            cell.imageView.image = [UIImage imageNamed:@"home_select_icon"];
			vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"Home"];
			break;
			
		case 1:
            cell.imageView.image = [UIImage imageNamed:@"my_coupons_select_icon"];
			vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"MyCoupons"];
			break;
			
		case 2:
            cell.imageView.image = [UIImage imageNamed:@"my_points_select_icon"];

            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"MyPoints"];
			break;
			
		case 3:
            cell.imageView.image = [UIImage imageNamed:@"my_rewards_select_icon"];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"MyRewards"];
            break;
            
        case 4:
            cell.imageView.image = [UIImage imageNamed:@"share_select_icon"];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"Share"];
            break;

        case 5:
            cell.imageView.image = [UIImage imageNamed:@"terms_and_conditions_select_icon"];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"Terms&Conditions"];
            break;

        case 6:
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileViewController"];
            break;
            
        case 7:
            [self logout];
            break;


	}
	
	[[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
															 withSlideOutAnimation:self.slideOutAnimationEnabled
																	 andCompletion:nil];
}
-(void)logout{
   

}

- (IBAction)btnProfile:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"Profile"];
    
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                             withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                     andCompletion:nil];
   
}

- (IBAction)btnLogout:(id)sender {
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *vc ;
    
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    [defs setObject:@"NO" forKey:@"isLogin"];
    [CommonFunctions userLoginDetail:nil];
    [defs synchronize];
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"Login"];
    
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                             withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                     andCompletion:nil];
}



@end
