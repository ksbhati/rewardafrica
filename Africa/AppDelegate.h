//
//  AppDelegate.h
//  Africa
//
//  Created by Kunal Singh Bhati on 28/12/15.
//  Copyright © 2015 Kunal Singh Bhati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "LeftMenuViewController.h"
#import "SlideNavigationController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,MBProgressHUDDelegate>

@property (strong, nonatomic) UIWindow *window;
@property NSString *fromLogin,*loginType,*valuesForMerchant;
@property NSMutableArray *arrMerchantIds;

@property BOOL flagForFront,isRegistered,fromProfileScreen;

@end

